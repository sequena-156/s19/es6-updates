let a = 2;
	const myNum = a ** 3;

let messageA = 'The cube of '
let messageB = ' is '


	console.log(messageA + a + messageB + myNum);

// address
const address = ['258 Washington Ave NW', 'California ', '90011']

// road, state, zipcode item no.6

const [road, state, zipcode] = address
console.log(`I live at ${road}, ${state} ${zipcode}`)

// item no.7

const wildLolong = {
	reptile: 'saltwater crocodile',
	measurement: '20ft 3 in.',
	weight: '1075 kgs'
}

//item no.8
// destructuring (object literals)
const {reptile, measurement, weight} = wildLolong;
console.log(`Lolong was a ${reptile}. He weighed at ${weight} with a measurement of ${measurement}`)

//item no. 9, 10, 11
// using forEach

let countNum = [1, 2, 3, 4, 5]

countNum.forEach(function(count){
	console.log(`${count}`);
})

// using arrow function

countNum.forEach((count)=>{
	console.log(`${count}`);
})


// reduce array method
let numbers = [1, 2, 3, 4, 5]

let iteration = 0;


const reduceNumber = numbers.reduce(function(x,y){
					++iteration;
					return x+y
})
console.log('reduced number using array method ' + reduceNumber);


// using arrow method 
const reducedNumber = numbers.reduce((x,y)=>{++iteration;return x+y})
console.log('reduced number using arrow method ' + reducedNumber)


// item no. 12

class dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new dog();
console.log(myDog)

// creating an instance on age
myDog.name = 'Lokis';
myDog.age = 5;
myDog.breed = 'Golden Retriever';

const myNewDog = new dog('Lucky', 7, 'Golden Retriever');
console.log(myNewDog);